﻿using System.Linq;
using System.Web.Mvc;
using CRUD.Models;

namespace CRUD.Controllers
{
    public class ReportController : Controller
    {
        Context db = new Context();
      
        public ActionResult Report()
        {
            var row = db.Database.SqlQuery<Report>("spReport").ToList();
            return View(row);
        }
    }
}