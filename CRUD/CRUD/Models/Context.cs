﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CRUD.Models
{
    public class Context : DbContext
    {
        public Context() : base ("name=Connection")
        {

        }

        public DbSet<Category> categories { get; set; }

        public DbSet<Product> products { get; set; }

        public DbSet<User> users { get; set; }

        public DbSet<Roles> roles { get; set; }
    }
}