﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JWT_Authentication.Models
{
    [Serializable]
    public class JWTAuthResponse
    {

        public string token { get; set; }
        public string username { get; set; }

        public int expires_in { get; set; }
    }
}