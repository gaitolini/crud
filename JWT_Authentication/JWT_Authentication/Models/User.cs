﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JWT_Authentication.Models
{

    [Table("Credentials")]
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string username { get; set; }

        [DataType(DataType.Password)]
        public string password { get; set; }


    }
}