﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JWT_Authentication.Models
{
    public class Context : DbContext
    {
        public Context() : base ("name=Connection")
        {

        }

        public DbSet<User> users { get; set; }
        public DbSet<Product> products { get; set; }
    }
}