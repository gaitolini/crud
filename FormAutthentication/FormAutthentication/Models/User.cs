﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FormAutthentication.Models
{
    [Table("Credentials")]
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [DisplayName("Username")]
        public string username { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string password { get; set; }

    }
}